
/**
 * @file
 * Redirect to front page after a set timeout
 */

$(document).ready(function() {
  setTimeout(function() {
    window.location = Drupal.settings.ucountSignin.baseURL +
                      Drupal.settings.basePath +
                      Drupal.settings.ucountSignin.redirectPath;
  }, Drupal.settings.ucountSignin.timeout);
});
