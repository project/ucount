
ABOUT
-----

This module is being developed and maintained by DANEnet, a non-profit located
in Madison, WI. It provides a simple and powerful way for organizations to
track attendees of events/activities using on-site kiosks and a central 
server running Drupal. The server can either be restricted to a local network 
or be open to the internet to allow off-site sign-ins.

The concept is simple. In our use-case, the system provides each non-admin
user a 12-digit UPC barcode printed on ID cards. They can then use the sign-in
form to scan this ID card and then select an activity they are attending. Their
barcode is associated with a node and is recorded, along with the activity node
and a timestamp, after successfully completing the sign-in form. Admin users 
can then sign-in to the system using the normal Drupal user form and
generate reports of attendees which can be broken down by time and activity.

There is a grave need for such a system for many Wisconsin non-profits and it
is DANEnet's hope that by providing this module on drupal.org we can also 
help non-profits world-wide. We also welcome anyone interested to help
out with development and maintenance.

SYSTEM OVERVIEW
---------------
// TO-DO

INSTALL
-------
// TO-DO