<?php

/**
 * @file
 * Example module that demonstrates the use of ucount_reports.module hooks.
 *
 * @see ucount_reports.module
 */
 
/**
 * Implementation of hook_menu().
 */ 
function ucr_example_menu() {
  $items['ucount/reports/example'] = array(
    'title' => 'Example uCount Report',
		'page callback' => 'ucr_example_report',
		'access arguments' => array('view ucount reports'),
		'type' => MENU_CALLBACK,
  );
  
  return $items;
}

/**
 * Implementation of hook_ucount_reports_info().
 */
function ucr_example_ucount_reports_info() {
  return array(
    array(
      'title' => t('Example uCount Report'),
      'description' => t('A demonstration of the report system.'),
      'path' => 'ucount/reports/example',
    ),
  );
}

/**
 * Display an example report for the uCount system.
 */
function ucr_example_report() {
  // This page callback will process and display the report after the user
  // clicks the report link.
  return '<p>'. t('This is a report!') .'</p>';
}
