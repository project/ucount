<?php

/**
 * @file
 * Test ucount_content module.
 */
 
/**
 * Functional tests for the uCount Content install routines.
 */
class UcountContentTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => t('uCount Content install'),
      'description' => t('Tests install routines.'),
      'group' => t('uCount'),
    );
  }

  public function setUp() {
    parent::setUp('content',
                  'content_copy',
                  'nodereference',
                  'number',
                  'optionwidgets',
                  'fieldgroup',
                  'text',
                  'token',
                  'content_taxonomy',
                  'content_taxonomy_options',
                  'date_api',
                  'date_timezone',
                  'date',
                  'addresses',
                  'addresses_cck',
                  'ucount_content');
  }
  
  /**
   * Tests if status vocabulary and terms correctly installed.
   */
  public function testInstalledStatusVocabulary() {
    // Test status vocabulary
    $status_vocab = db_result(db_query("SELECT name 
                                        FROM {vocabulary}
                                        WHERE vid = %d",
                                        variable_get('ucount_content_status_vid', -1)));
                                        
    $this->assertEqual($status_vocab, 'uCount Participant Status',
                       t('Make sure status \'vid\' represents the correct vocabulary.'));
    
    // Test status terms
    $status_terms = db_result(db_query("SELECT vid
                                        FROM {term_data}
                                        WHERE name = '%s'",
                                        'Allowed to sign-in'));
    
    $this->assertIdentical($status_terms, variable_get('ucount_content_status_vid', -1),
                           t('Make sure status terms exist and are in the correct vocabulary.'));
  }
  
  /**
   * Tests if region vocabulary and terms correctly installed.
   */
  public function testInstalledRegionsVocabulary() {    
    // Test regions vocabulary
    $regions_vocab = db_result(db_query("SELECT name 
                                         FROM {vocabulary}
                                         WHERE vid = %d",
                                         variable_get('ucount_content_regions_vid', -1)));
    
    $this->assertEqual($regions_vocab, 'uCount Residence Regions',
                       t('Make sure regions \'vid\' represents the correct vocabulary.')); 
  }
  
  /**
   * Tests if node types correctly installed.
   */
  public function testInstalledNodeTypes() {
    // Check that node types were installed
    $node_types_query = db_query("SELECT type
                                  FROM {node_type}");
    $node_types = array();
    while($node_type = db_fetch_object($node_types_query)) {
      $node_types[] = $node_type->type;
    }
    
    $part = in_array('participant', $node_types);
    $act  = in_array('activity', $node_types);
    $vol  = in_array('volunteer_hour_option', $node_types);
    $sign = in_array('signin_record', $node_types);
    
    $this->assertTrue($part, t('Make sure participant node type is installed.'));
    $this->assertTrue($act, t('Make sure activity node type is installed.'));
    $this->assertTrue($vol, t('Make sure volunteer hour option node type is installed.'));
    $this->assertTrue($sign, t('Make sure signin record node type is installed.')); 
  }
  
  /**
   * Tests if node types locked during install.
   */
  public function testInstalledAndLockedNodeTypes() {
    // Test locked status of node-types
    $locked_nodes_query = db_query("SELECT type 
                                    FROM {node_type} 
                                    WHERE locked = 1");
    $locked_nodes = array();
    while ($locked_type = db_fetch_object($locked_nodes_query)) {
      $locked_nodes[] = $locked_type->type;
    }

    $locked_part = in_array('participant', $locked_nodes);
    $locked_act  = in_array('activity', $locked_nodes);
    $locked_vol  = in_array('volunteer_hour_option', $locked_nodes);
    $locked_sign = in_array('signin_record', $locked_nodes);

    $this->assertTrue($locked_part, t('Make sure participant node type is locked.'));
    $this->assertTrue($locked_act, t('Make sure activity node type is locked.'));
    $this->assertTrue($locked_vol, t('Make sure volunteer hour option node type is locked.'));
    $this->assertTrue($locked_sign, t('Make sure signin record node type is locked.')); 
  }
  
  public function tearDown() {
    parent::tearDown();
  }
}
