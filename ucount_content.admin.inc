<?php

/**
 * @file
 * Relate content type to internal uCount content name.
 */

//---------------------------------------------------------------------------//
// MENU CALLBACKS (FORMS)                                                    //
//---------------------------------------------------------------------------//

/**
 * Provide administration settings form for content type configuration.
 *
 * @ingroup forms
 */
function ucount_content_admin_settings() {
  /*$node_types = array_map('check_plain', node_get_types('names'));
  
  $form['ucount_content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('uCount Content Types'),
  );
  
  $form['ucount_content_types']['ucount_content_participant'] = array(
    '#type' => 'select',
    '#title' => t('Participant Type'),
    '#description' => t('uCount Sign-in driver modules and reports will use this content type when creating or looking up participants.'),
    '#options' => $node_types,
    '#default_value' => variable_get('ucount_content_participant', 'participant'),
  );
  
  $form['ucount_content_types']['ucount_content_activity'] = array(
    '#type' => 'select',
    '#title' => t('Activity Type'),
    '#description' => t('uCount Sign-in driver modules and reports will use this content type when creating or looking up activities.'),
    '#options' => $node_types,
    '#default_value' => variable_get('ucount_content_activity', 'activity'),
  );
  
  $form['ucount_content_types']['ucount_content_volunteer_hour_option'] = array(
    '#type' => 'select',
    '#title' => t('Volunteer Hour Options Type'),
    '#description' => t('uCount Sign-in driver modules and reports will use this content type when creating or looking up volunteer hour options.'),
    '#options' => $node_types,
    '#default_value' => variable_get('ucount_content_volunteer_hour_option', 'volunteer_hour_option'),
  );
  
  $form['ucount_content_types']['ucount_content_signin_record'] = array(
    '#type' => 'select',
    '#title' => t('Sign-in Record Type'),
    '#description' => t('uCount Sign-in driver modules and reports will use this content type when creating or looking up sign-in records.'),
    '#options' => $node_types,
    '#default_value' => variable_get('ucount_content_signin_record', 'signin_record'),
  );*/
  
  $part_values = array('-1' => t('None'));
  $part_title_query = db_query(db_rewrite_sql("SELECT n.title, n.nid FROM {node} n WHERE n.type = 'participant' ORDER BY n.title ASC"));
  while ($part_title_result = db_fetch_object($part_title_query)) {
    $part_values[$part_title_result->nid] = $part_title_result->title;
  }
  
  $act_values = array('-1' => t('None'));
  $act_title_query = db_query(db_rewrite_sql("SELECT n.title, n.nid FROM {node} n WHERE n.type = 'activity' ORDER BY n.title ASC"));
  while ($act_title_result = db_fetch_object($act_title_query)) {
    $act_values[$act_title_result->nid] = $act_title_result->title;
  }
  
  $form['ucount_content_spec_nodes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Special uCount System Content'),
  );
  
  $form['ucount_content_spec_nodes']['ucount_content_guest_nid'] = array(
    '#type' => 'select',
    '#title' => t('Guest Participant'),
    '#options' => $part_values,
    '#default_value' => variable_get('ucount_content_guest_nid', -1),
  );
  
  $form['ucount_content_spec_nodes']['ucount_content_sign_out_nid'] = array(
    '#type' => 'select',
    '#title' => t('Sign-out Activity'),
    '#options' => $act_values,
    '#default_value' => variable_get('ucount_content_sign_out_nid', -1),
  );
  
  return system_settings_form($form);
}
