<?php

/**
 * @file
 * Example module that demonstrates the use of ucount_signin.module hooks.
 *
 * @see ucount_signin.module
 */

/**
 * Implementation of hook_theme().
 */
function ucs_example_theme() {
  return array(
    'ucs_example_confirm' => array(
      'arguments' => array('message' => NULL),
    ),
    
    'ucs_example_storage' => array(
      'arguments' => array('storage' => array()),
    ),
  );
}

/**
 * Implementation of hook_ucount_signin_info().
 */
function ucs_example_ucount_signin_info() {
  // Return an array with the system name of your module as a key, and
  // a user friendly name as a value. Value should be wrapped in t().
  return array(
    'ucs_example' => t('uCount Example Sign-in Driver'),
  );
}

/**
 * Implementation of hook_ucountapi().
 */
function ucs_example_ucountapi($op, $form_value = NULL) {
  switch ($op) {
    case 'participant form':
      $form['example_part'] = array(
        '#type' => 'textfield',
        '#title' => t('Example Participant Entry'),
        '#description' => t('A description of this example.'),
      );
      return $form;
      
    case 'activity form':
      $form['example_act'] = array(
        '#type' => 'textfield',
        '#title' => t('Example Activity Entry'),
        '#description' => t('A description of this example.'),
      );
      
      $form['example_hours'] = array(
        '#type' => 'textfield',
        '#title' => t('Example Volunteer Hour Entry'),
        '#description' => t('This is also an example.'),
      );
      return $form;
      
    case 'signin confirmation':
      return theme('ucs_example_confirm', 
                   t('The example signin has finished!'));
    
    case 'participant validate':
      drupal_set_message(t('Validated participant value: @part',
                         array('@part' => $form_value['example_part'])));
      break;
    
    case 'activity validate':
      drupal_set_message(t('Validated activity value: @act', array('@act' => $form_value['example_act'])));
                         
      drupal_set_message(t('Validated volunteer value: @vol', array('@vol' => $form_value['example_hours'])));
      break;
      
    case 'signin submit':
      drupal_set_message(t('Form values: ') . theme('ucs_example_storage', $form_value));
      _ucs_example_create_signin();
      break;
  }
}

/**
 * Theme messages for the confirmation page.
 *
 * @see ucs_example_ucountapi()
 */
function theme_ucs_example_confirm($message) {
  return '<p>'. $message .'</p>';
}

/**
 * Theme form storage output for the confirmation page.
 */
function theme_ucs_example_storage($storage) {
  return '<br /><pre>'. print_r($storage, TRUE) .'</pre>';
}

/**
 * Create a fake sign-in node.
 */
function _ucs_example_create_signin() {
  // Fake participant node
  $participant = (object)array('nid' => 2, 'title' => 'test participant');
  
  // Fake activity node
  $activity = (object)array('nid' => 3, 'title' => 'test activity');
  
  $hours = 10;
  $date = time();
  
  ucount_signin_create($participant, $activity, $hours, $date);
}
