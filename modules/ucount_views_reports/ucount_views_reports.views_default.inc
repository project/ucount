<?php

/**
 * @file
 * Imports the views from the views directory in the ucount_views_report module.
 */

/**
 * Implementation of hook_views_default_views().
 */
function ucount_views_reports_views_default_views() {
  $views = array();
  
  $module_path = drupal_get_path('module', 'ucount_views_reports');
  foreach (file_scan_directory($module_path .'/views', '\.view$') as $view_file) {
    $view_str = file_get_contents($view_file->filename);
    eval($view_str);
    
    if (isset($view)) {
      $views[$view->name] = $view;
      unset($view);
    }
  }
  
  return $views;
}
