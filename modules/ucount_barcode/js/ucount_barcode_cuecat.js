
/**
 * @file
 * Handles client side decoding on CueCat cipher text.
 *
 * This is a client side barcode driver. It is required to define the
 * ucountBarcode.driver.process(data) method in this driver. This method
 * gets called whenever the user finishes their input.
 *
 * An optional method can also be defined:
 * ucountBarcode.driver.inputFinished(e). This method should return true
 * when the user input has been completed. The e parameter is the same
 * event parameter inside the .keypress event.
 */

/**
 * Decode CueCat cipher text to a UPC-A barcode.
 *
 * JavaScript version of its PHP counter-part.
 *
 * @see ucount_barcode_decuecat().
 */
ucountBarcode.driver.decuecat = function(barcode) {
  // Return the barcode if it isn't valid cuecat cipher text
  if (barcode.indexOf('.') < 0) {
    return barcode;
  }

  // Extract the barcode section of the cuecat cipher text
  barcode = barcode.split('.');
  if (barcode.length != 5) {
    return false;
  }

  barcode = barcode[3];

  var decode = [];

  // Cipher mamp A
  decode['C3'] = 0;
  decode['CN'] = 1;
  decode['Cx'] = 2;
  decode['Ch'] = 3;
  decode['D3'] = 4;
  decode['DN'] = 5;
  decode['Dx'] = 6;
  decode['Dh'] = 7;
  decode['E3'] = 8;
  decode['EN'] = 9;

  // Cipher map B
  decode['n'] = 0;
  decode['j'] = 1;
  decode['f'] = 2;
  decode['b'] = 3;
  decode['D'] = 4;
  decode['z'] = 5;
  decode['v'] = 6;
  decode['r'] = 7;
  decode['T'] = 8;
  decode['P'] = 9;

  // Cipher map C
  decode['Z'] = 0;
  decode['Y'] = 1;
  decode['X'] = 2;
  decode['W'] = 3;
  decode['3'] = 4;
  decode['2'] = 5;
  decode['1'] = 6;
  decode['0'] = 7;
  decode['7'] = 8;
  decode['6'] = 9;

  var cipherBlock = false;
  var key = '';
  var plainText = '';
  for (var i = 0; i < barcode.length; i++) {
    if (cipherBlock) {
      key = cipherBlock + barcode[i];
      if (decode[key] !== undefined) {
        plainText += decode[key];
        cipherBlock = false;
        continue;
      }
      else {
        plainText += decode[key];
        cipherBlock = false;
      }
    }

    if (barcode[i] == 'C' || barcode[i] == 'D' || barcode[i] == 'E') {
      cipherBlock = barcode[i];
      continue;
    }

    if (decode[barcode[i]] !== undefined) {
      plainText += decode[barcode[i]];
    }
  }

  return plainText;
};

/**
 * Implementation of the ucountBarcode.driver.process() method.
 *
 * @see ucount_barcode_cuecat.js
 */
ucountBarcode.driver.process = function(data) {
  data = ucountBarcode.driver.decuecat(data);
  
  if (data.length == 12) {
    data = "0" + data;
  }

  return data;
}

/**
 * Implementation of the ucountBarcode.driver.inputFinished() method.
 *
 * @see ucount_barcode_cuecat.js
 */
ucountBarcode.driver.inputFinished = function(e, element) {
  return (e.which == 13);
}
