
/**
 * @file
 * Handles client side decoding on CueCat cipher text.
 *
 * This is a client side barcode driver. It is required to define the
 * ucountBarcode.driver.process(data) method in this driver. This method
 * gets called whenever the user finishes their input.
 *
 * An optional method can also be defined:
 * ucountBarcode.driver.inputFinished(e). This method should return true
 * when the user input has been completed. The e parameter is the same
 * event parameter inside the .keypress event.
 */

/**
 * Implementation of the ucountBarcode.driver.process() method.
 *
 * @see ucount_barcode_cuecat.js
 */
ucountBarcode.driver.process = function(data) {
  // Pad with a 0 if the barcode is 12 digits, since
  // USB CueCats seem to ignore leading 0's in EAN-13
  // barcodes.
  if (data.length == 12) {
    data = "0" + data;
  }

  return data;
}

/**
 * Implementation of the ucountBarcode.driver.inputFinished() method.
 *
 * @see ucount_barcode_cuecat.js
 */
ucountBarcode.driver.inputFinished = function(e, element) {
  return (e.which == 13);
}
