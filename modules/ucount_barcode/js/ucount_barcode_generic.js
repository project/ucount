
/**
 * @file
 * Handles client side processing of generic barcodes.
 */

/**
 * Keep track of key presses while the user enters input.
 */
ucountBarcode.driver.keyPressCount = [];

/**
 * Implementation of the ucountBarcode.driver.process() method.
 *
 * @see ucount_barcode_cuecat.js
 */
ucountBarcode.driver.process = function(data) {
  if (data.length == 11) {
    data = '0'+ data;
  }
  else if (data.length == 13) {
    data = data.substr(0, 12);
  }

  return data;
}

/**
 * Implementation of the ucountBarcode.driver.inputFinished() method.
 *
 * @see ucount_barcode_cuecat.js
 */
ucountBarcode.driver.inputFinished = function(e, element) {
  var elementID = $(element).attr('id');
  var count = 0;
  
  if (ucountBarcode.driver.keyPressCount[elementID] === undefined) {
    count = ucountBarcode.driver.keyPressCount[elementID] = 0;
  }
  else {
    count = ++ucountBarcode.driver.keyPressCount[elementID];
  }

  if (e.which == 13 || count == 12) {
    return true;
  }

  return false;
}
