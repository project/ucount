<?php

/**
 * @file
 * Contains the admin form for the ucount_barcode module.
 */

/**
 * Provide administration settings form for barcode configuration.
 *
 * @ingroup forms
 */
function ucount_barcode_admin_settings() {
  $form['ucount_barcode_drivers'] = array(
    '#type' => 'fieldset',
    '#title' => t('uCount Barcode Drivers'),
  );

  $form['ucount_barcode_drivers']['ucount_barcode_driver'] = array(
    '#type' => 'select',
    '#title' => t('Barcode Driver'),
    '#description' => t('Select a driver that corresponds to the barcode scanning hardware you are using at your kiosks.'),
    '#options' => array(
      'cuecat' => t('CueCat'),
      'generic' => t('Generic'),
    ),
    '#default_value' => variable_get('ucount_barcode_driver', 'cuecat'),
  );

  return system_settings_form($form);
}
