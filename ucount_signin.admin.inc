<?php

/**
 * @file
 * Configure settings for uCount Sign-in, such as driver module.
 */
 
//---------------------------------------------------------------------------//
// MENU CALLBACKS (FORMS)                                                    //
//---------------------------------------------------------------------------//

/**
 * Provide administration settings form for uCount Sign-in driver module 
 * configuration.
 *
 * @ingroup forms
 */
function ucount_signin_admin_settings() {
  $form['ucount_signin_js'] = array(
    '#type' => 'fieldset',
    '#title' => t('uCount Sign-in JavaScript Settings'),
  );
  
  $form['ucount_signin_js']['ucount_signin_js_timeout'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable JavaScript timeouts on sign-in form.'),
    '#description' => t('If checked, the sign-in form will timeout at configurable intervals.'),
    '#default_value' => variable_get('ucount_signin_js_timeout', 1),
  );
  
  $form['ucount_signin_js']['ucount_signin_js_confirmation'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation Timeout Length'),
    '#description' => t('Set the timeout, in milliseconds, for the confirmation page.'),
    '#default_value' => variable_get('ucount_signin_js_confirmation', 5000),
  );
  
  $form['ucount_signin_js']['ucount_signin_js_regular'] = array(
    '#type' => 'textfield',
    '#title' => t('Normal Timeout Length'),
    '#description' => t('Set the timeout, in milliseconds, for all other pages.'),
    '#default_value' => variable_get('ucount_signin_js_regular', 60000),
  );
  
  $form['ucount_signin_js']['ucount_signin_js_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Timeout Redirect Path'),
    '#description' => t('Set the Drupal path that you would like to redirect to (i.e. node/43). Use <front> if want to redirect to the front page.'),
    '#default_value' => variable_get('ucount_signin_js_path', 'ucount/signin'),
  );
  
  $form['ucount_signin_driver_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('uCount Sign-in Driver Settings'),
  );
  
  $form['ucount_signin_driver_settings']['ucount_signin_driver'] = array(
    '#type' => 'select',
    '#title' => t('uCount Sign-in Driver Module'),
    '#options' => _ucount_signin_drivers_options(),
    '#default_value' => variable_get('ucount_signin_driver', 0),
  );
  
  $form['ucount_signin_display_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('uCount Sign-in Display Options'),
  );
  
  $form['ucount_signin_display_options']['ucount_signin_bigtext'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display large font on sign-in form'),
    '#default_value' => variable_get('ucount_signin_bigtext', 0),
  );
  
  return system_settings_form($form);
}

//---------------------------------------------------------------------------//
// PUBLIC UTILITY FUNCTIONS                                                  //
//---------------------------------------------------------------------------//

/**
 * Retrieve all modules that identify as an uCount Sign-in Driver.
 */
function _ucount_signin_drivers_options() {
  $driver_modules = module_invoke_all('ucount_signin_info');
  
  if (!empty($driver_modules)) {
    return array(0 => t('Disable uCount Sign-in')) + $driver_modules;
  }
  else {
    return array(0 => t('Disable uCount Sign-in'));
  }
}
